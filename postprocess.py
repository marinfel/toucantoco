
class PostProcessor:
    """
    The aim of this class is to handle postprocessing for a given query.
    When this query has specified postprocess options,
    these options are run sequencially.
    My idea was to have one method per possible
    postprocess key (argmax, ...). Each method
    is run on the result stored in data attribute, and the postprocessed result is the one finally
    added to HTTP responses.
    """

    def __init__(self, query_res, query):
        self._data = query_res  #: Query result
        self._db_query = query  #: Query object, useful to get the postprocess
                                #: options (there are other ways).

    @property
    def data(self):
        """

        data attribute property

        :return: The _data attribute, which is a dict with the query result to postprocess
        """
        return self._data

    @property
    def db_query(self):
        """

        db_query attribute property

        :return: the _db_query attribute, which is a Query object
        """
        return self._db_query

    def argmax(self, params):
        """

        Only postprocess method implemented for this test.

        :param params: The parameters of the postprocess (a dictionary, found in the config file next to the query)
        :return: Update the _data attribute
        """
        for column in params:
            self._data = max(self._data, key=lambda el: el[column])

    def manage_postprocess(self):
        """

        Handle postprocess options for the _db_query object on _data results.
        Execute sequencially all postprocess methods found in config file for the Query object
        in the _db_query attribute.

        :return: The postprocessed results
        """
        for method in self._db_query.postprocess:
            method_name = list(method.keys())[0]
            method_params = method.values()
            to_call = getattr(self, method_name,
                              'Postprocess %s is not implemented.' % method_name)
            if callable(to_call):
                to_call(method_params)
            else:
                print(to_call)
        return self._data
