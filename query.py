import postprocess
import uuid


class Query:
    """
    The Query class allow to keep all useful information for a query
    such as its parameters and, if necessary, the list of
    postprocess options to handle.
    """

    def __init__(self, params, postprocess):
        self._q_uuid = str(uuid.uuid4())[:5]
        self._params = params
        self._postprocess = postprocess

    @property
    def q_uuid(self):
        """

        q_uuid attribute property

        :return: The _q_uuid attribute, which is a unique string identifier
        """
        return self._q_uuid

    @property
    def params(self):
        """

        params attribute property

        :return: The _params attribute, which is a dictionary of query parameters
        """
        return self._params

    @property
    def postprocess(self):
        """

        postprocess attribute property

        :return: The _postprocess attribute, which is a dictionary of found postprocess options in file for the query
        """
        return self._postprocess

    def run(self, db, params=None):
        """

        Method that runs the query in specified database

        :param db: The database used to run the query
        :param params: Optional parameters passed in the POST request
        :return: The post processed results of the query
        """
        params = params or self._params
        if isinstance(params, list):
            # This is an aggregate query (or several queries)
            if all(isinstance(el, list) for el in params):
                # These are multiple aggregate queries
                results = ''.join(str(db.mycoll.aggregate(elem).next())
                                  for elem in params)
            else:
                results = db.mycoll.aggregate(params)
        elif isinstance(params, dict):
            results = db.mycoll.find(params)
        results = [res for res in results]
        post_processor = postprocess.PostProcessor(results, self)
        return post_processor.manage_postprocess()

