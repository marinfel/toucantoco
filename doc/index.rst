.. toucan documentation master file, created by
   sphinx-quickstart on Sat Feb 24 11:41:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to toucan's documentation!
==================================

.. toctree::
   :maxdepth: 10
   :caption: Contents:

   app
   config
   postprocess
   query

.. autoclass:: Query
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
