#!flask/bin/python
from flask import Flask, jsonify, abort, request, json, Response
from functools import wraps
from config import Config
from pymongo import MongoClient
from copy import deepcopy
from bson import json_util


class RestApi:
    """

    This class represents an API using a specific config file.

    """
    def __init__(self, config_file):
        """

        API constructor. Allows to generate apps from different config files.

        :param config_file: The config file path to parse (and to get the queries from).
        """
        self._db_client = MongoClient('localhost', 27017)
        self._db = self._db_client.mytest
        self._app = Flask(__name__)
        self._config = Config(config_file)

    @property
    def app(self):
        """

        _app attribute property

        :return: A Flask app object
        """
        return self._app

    @property
    def config(self):
        """

        _config attribute property

        :return: A Config object
        """
        return self._config

    def run(self):
        """

        Defines the routes for _app Flask application and run it.

        """
        app = self._app

        # def check_auth(username, password):
        #     """
        #
        #     This function is called to check if a username /
        #     password combination is valid.
        #     It should check them from a dedicated DB with user credentials stored.
        #
        #     """
        #     return username == 'user' and password == 'pwd'
        #
        # def authenticate():
        #     """Sends a 401 response that enables basic auth"""
        #     return Response(
        #         'Could not verify your access level for that URL.\n'
        #         'Please authenticate', 401,
        #         {'WWW-Authenticate': 'Basic realm="Login Required"'})
        #
        # def auth_required(f):
        #     @wraps(f)
        #     def decorated(*args, **kwargs):
        #         auth = request.authorization
        #         if not auth or not check_auth(auth.username, auth.password):
        #             return authenticate()
        #         return f(*args, **kwargs)
        #     return decorated

        @app.route('/')
        def index():
            return "Root page, nothing to show there!"

        @app.route('/queries', methods=['GET'])
        # @auth_required
        def get_all_queries():
            """
            Get the list of all query IDs
            :return: List of all queries IDs generated from config file
            """
            queries = [q.q_uuid for q in self._config.queries]
            return jsonify(list(queries))

        @app.route('/query/<string:query_id>', methods=['GET'])
        # auth_required
        def run_query(query_id):
            if not query_id:
                abort(404)
            query = self._config.get_query_by_id(query_id)
            results = query.run(self._db)
            return jsonify({'query result': json.loads(
                json_util.dumps(results))
            })

        @app.route('/query/<string:query_id>', methods=['POST'])
        # @auth_required
        def run_post_query(query_id):
            """
            Run a query called with a POST request.
            Replaces Jinja2 templates ('{{my_filter}}') by post parameters
            corresponding value if passed.
            """
            query = self._config.get_query_by_id(query_id)
            if not query:
                abort(404)
            http_params = request.get_json()
            query_params = deepcopy(query.params)
            for key, val in query.params.items():
                # There is probably a way to do this with Jinja2 templates objects
                if val.startswith('{{') and val.endswith('}}'):
                    try:
                        query_params[key] = http_params[key]
                    except KeyError:
                        pass
            results = query.run(self._db, query_params)
            return jsonify({'query result': json.loads(
                json_util.dumps(results))
            })

        app.run(debug=True)


if __name__ == '__main__':
    RestApi('./backtechtest/config.cson').run()
