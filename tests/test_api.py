
import sys
sys.path.append('..')
import unittest
from app import RestApi
from bson import json_util
from flask import json, jsonify


class TestFlaskApi(unittest.TestCase):

    def setUp(self):
        self.api = RestApi('./config_test.cson')
        self.client = self.api.app.test_client()

    def test_parse_config(self):
        """

        Ensure that we parsed as many queries as we expected,
        knowing the exact number of queries in test config file.

        """
        config = self.api.config
        self.assertEqual(len(config.queries), 94, 'Parser should have fetched 94 DB queries in file')

    def test_queries_list_get(self):
        """

        Ensure that the /queries flask replies correctly to a GET request

        """
        with self.api.app.app_context():
            response = self.client.get('/queries')
            expected = json_util.dumps({'query_result': list([q.q_uuid for q in self.api.config.queries])})
            self.assertEqual(json.loads(response.get_data()), expected, 'Route should return all query ids')

    def tearDown(self):
        self.api.config.config.release()

if __name__ == '__main__':
    unittest.main()