
from query import Query
from biconfigs import Biconfigs, parser_cson
from copy import deepcopy


class Config:
    """
    The Config class is only used to parse the file and
    get the queries stored in it.
    It is instanciated at app launching and parses the passed file
    to instanciate Query objects with file data.
    """
    def __init__(self, file_path):
        """

        Initialize the configuration by parsing the file
        passed as parameter.
        Using Biconfigs, it should work for CSON as well as JSON files.

        :param file_path: A path to the CSON config file
        """
        try:
            self._config = Biconfigs(file_path) #: Config file stored as dict-like object
            self._queries = self.extract_queries(self._config) #: List of all parsed queries
        except Exception as e:
            print(""""%s is not a valid configuration file. Error was : %s""" % (file_path, e,))

    @property
    def config(self):
        """

        config attribute property

        :return: The _config attribute which is a dict-like object
        """
        return self._config

    @property
    def queries(self):
        """

        queries attribute property

        :return: The _queries attribute, which is a list of Query objects
        """
        return self._queries

    def extract_queries(self, data, queries=[]):
        """

        Extract all queries from config file.
        Queries are stored in a list of Query objects.

        :param data: Initially, this is the whole file (list of dictionaries).
                     Then we call the method recursively on sub data structures until
                     we find a query key. We do it for all the file data.
        :param queries: Already extracted queries in file.
                        It is updated at each recursive call when a query is found.
        :return: The list of all queries in the config file
        """
        if isinstance(data, dict):
            if data.get('query'):
                queries.append(Query(deepcopy(data['query']),
                                     deepcopy(data.get('postprocess', []))
                                     )
                               )
            else:
                for v in data.values():
                    self.extract_queries(v, queries)
        elif isinstance(data, list):
            for data_dict in data:
                self.extract_queries(data_dict, queries)
        return queries

    def get_query_by_id(self, q_uid):
        """

        Returns a query given its uuid

        :param q_uid: The ID of the query we want to get from _queries
        :return: The corresponding Query object
        """
        for q in iter(self._queries):
            if q.q_uuid == q_uid:
                return q
        return False