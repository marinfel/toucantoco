# My work on the Toucan Toco backend test

In this project, I tried to provide a solution to the subject I have been offered to work on.

## Subject
I have been given a large CSON file that contains, among other data,
some MongoDB queries.

The aim was to:

    * Parse this file to extract these queries
    * Provide a REST API to run these queries given a MongoDB database and collection.

I chose a solution that, according to me, make object roles very distinct and clean,
and keep the code readable and maintainable. Here is the architecture I implemented.


## Project architecture

    * config.py: Contains the Config class, that is used for the parsing
    * query.py: Defines the Query class, that represents a DB query fetched from the config.
    * postprocess.py: Defines a class whose role is to run the postprocess options that can be defined for a query.
    * app.py: Contains a short flask REST API, that instantiates a Config object
    in order to parse the given file and uses the Query objects generated to reply to HTTP requests. Only POST and GET requests are handled.

## Running the app


To run the app, you first need to install the requirements:
```bash
sudo pip install -r requirements.txt
```

Then you need to run the following command to get a running MongoDB database server instance.
```bash
docker run --rm -i -p 27017:27017 toucantoco/backtechtest
```

Finally, assuming Python 3.6 is installed:

```bash
python3.6 app.py
```

When we execute the app.py file, the configuration file is parsed and the flask application is run.
Queries are then available through the CONFIG global variable.
Each query method (GET, POST) are handled in a distinct flask route to keep the code readable.
I implemented three routes:

    * GET /queries: returns the list of all queries
    * GET /query/query_id: executes the query_id query with its parameters found in file
    * POST /query/query_id: executes the query_id query with parameters, replacing its parameters found
    in file by those passed in the request.
Queries are actually executed through a Query method, that calls the PostProcessor class to
handle optional postprocess options.

In order to be able to test the only postprocess method I implemented, I added an argmax key in the first query in config.cson. I checked that it worked with the HTTP request:

```bash
curl -X POST -H "Content-Type: application/json"  -d '{"my_key": {"$in": [1234, 5678]}}' localhost:5000/query/<ID of first query>
```

## Documentation
A details documentation of the whole project can be found id doc/_build/html/index.py
It's a Sphinx-generated page with useful information of classes and methods I have developed.

## Testing
I tried to implement unit tests, using the test_client() app method. All requests return a 404 error.
I didn't have the time to figure out why, but still I wanted to show the unit testing logic:
    * I Copied the config.cson file to get a test file (which could be different from the original one). The idea
    was to have an independant file, whose content is well known to make assertions on it.
    * I moved the Flask app logic in a class that instantiates the app from a configuration file.
    This allows me to instantiate an app from the test config file.
    * Finally I developped two documented tests.
We could add number of tests, such as:
    * Passing tests: check the response of a correct POST request
    * Non passing tests: check that a POST request without a query specified returns a 404 error
    * Test postprocess options
To run the tests:
```bash
python3.6 test_api.py
```
But once again I didn't got them to work.

## Authentication
I implemented a short solution for authentication. It uses the werkzeug authorization header and the werkzeug WWWAuthenticate class
to check if credentials are valid for a user. If not, it opens a form for him to log in.
These checks and operations are implemented in a simple decorator.
I commented it because it prevented curl tests, but it can be uncommented if you want to try a simple GET request in a navigator.
